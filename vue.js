const app = new Vue({
  el: "#app",
  data() {
    return {
      gato: [],
      tex: "",
      cont: 0,
      mostrar: true,
      MostrarAjustes: false,
      juego: [
        {
          nombre: "PAC-MAN",
          seleccionado: false
        },
        {
          nombre: "GATO",
          seleccionado: false
        },
        {
          nombre: "BUSCAMINAS",
          seleccionado: false
        },
        {
          nombre: "FLAPPY BIRD",
          seleccionado: false
        }
      ]
    };
  },
  computed: {},
  methods: {
    gat(num) {
      if (this.gato[num] == null) {
        if (this.cont % 2 == 0) {
          Vue.set(this.gato, num, "O");
        } else {
          Vue.set(this.gato, num, "X");
        }
        this.cont++;
        this.verificar();
      }
    },
    verificar() {
      let comp = [];
      for (i = 0; i <= 8; i++) {
        if (this.gato[i] == "X") {
          comp[i] = 1;
        } else if (this.gato[i] == "O") {
          comp[i] = -1;
        }
      }
      let verHorizontal = comp[0] + comp[1] + comp[2];
      let verHorizontal2 = comp[3] + comp[4] + comp[5];
      let verHorizontal3 = comp[6] + comp[7] + comp[8];
      let verVerticall1 = comp[0] + comp[3] + comp[6];
      let verVerticall2 = comp[1] + comp[4] + comp[7];
      let verVerticall3 = comp[2] + comp[5] + comp[8];
      let verDiagonal1 = comp[0] + comp[4] + comp[8];
      let verDiagonal2 = comp[2] + comp[4] + comp[6];
      if (
        verHorizontal == -3 ||
        verHorizontal2 == -3 ||
        verHorizontal3 == -3 ||
        verVerticall1 == -3 ||
        verVerticall2 == -3 ||
        verVerticall3 == -3 ||
        verDiagonal1 == -3 ||
        verDiagonal2 == -3
      ) {
        alert("El jugador O ha ganado!");
        for (i = 0; i <= 8; i++) {
          Vue.set(this.gato, i, null);
        }
      } else if (
        verHorizontal == 3 ||
        verHorizontal2 == 3 ||
        verHorizontal3 == 3 ||
        verVerticall1 == 3 ||
        verVerticall2 == 3 ||
        verVerticall3 == 3 ||
        verDiagonal1 == 3 ||
        verDiagonal2 == 3
      ) {
        alert("El jugador X ha ganado!");
        for (i = 0; i <= 8; i++) {
          Vue.set(this.gato, i, null);
        }
      }
      let empate = true;
      for (j = 0; j <= 8; j++) {
        if (this.gato[j] == null) {
          console.log(this.gato[j]);
          empate = false;
        } else {
        }
      }
      if (empate) {
        alert("Empate!");
        for (i = 0; i <= 8; i++) {
          Vue.set(this.gato, i, null);
        }
      }
    },
    ver(nombre) {
      alert(nombre);
    },
    MeteSaca() {
      document.getElementsByClassName("burger").classlist.toggle("Mete");
    }
  }
});
